# PulseDetection

  At the beginning, the user must center his face to make facial recognition and start sampling colors. There are three buttons on the home screen. The plot button is red at the beginning and does not work, after 784 sampling frames it becomes green and the user can press it to display the results. The erase button deletes all the samples that already exist and starts sampling at the beginning, this is done in the event of a mistake by the user so that there is no altered signal. The ChangeCamera button changes from the front camera to the bottom camera and vice versa. There is also a text above the buttons which tells the average of the green value per frame.
  
  Once the plot button is pressed, the display shows a graph that is the final signal, a heart image, and next to the imaging heart rate pulses that were calculated.
