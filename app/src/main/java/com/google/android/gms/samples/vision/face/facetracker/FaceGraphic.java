/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gms.samples.vision.face.facetracker;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import com.google.android.gms.samples.vision.face.facetracker.ui.camera.GraphicOverlay;
import com.google.android.gms.vision.face.Face;
import java.util.ArrayList;

import static com.google.android.gms.samples.vision.face.facetracker.MyFaceDetector.getBitmap;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;
    public Bitmap frame;
    public MyFaceDetector as;
    public static int framecnt;
  //  public static ArrayList<Float> redf = new ArrayList<>();
    public static ArrayList<Float> greenf = new ArrayList<>();
  //  public static ArrayList<Float> bluef = new ArrayList<>();

    private static final int COLOR_CHOICES[] = {
        Color.BLUE,
        Color.CYAN,
        Color.GREEN,
        Color.MAGENTA,
        Color.RED,
        Color.WHITE,
        Color.YELLOW
    };
    private static int mCurrentColorIndex = 0;

    private Paint mFacePositionPaint;
    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;
    private int mFaceId;
    private float mFaceHappiness;

    FaceGraphic(GraphicOverlay overlay) {
        super(overlay);

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);

        mIdPaint = new Paint();
        mIdPaint.setColor(selectedColor);
        mIdPaint.setTextSize(ID_TEXT_SIZE);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
    }

    void setId(int id) {
        mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;
        if (face == null) {
            return;
        }
        frame = getBitmap();

        // canvas.drawBitmap(frame,0,0,mBoxPaint);
        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);
        canvas.drawCircle(x, y, FACE_POSITION_RADIUS, mFacePositionPaint);

        // Draws a bounding box around the face.
        float xOffset = scaleX(face.getWidth() / 2.0f);
        float yOffset = scaleY(face.getHeight() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        canvas.drawRect(left, top, right, bottom, mBoxPaint);

        //canvas.drawText("id: " + mFaceId, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
        //canvas.drawText("happiness: " + String.format("%.2f", face.getIsSmilingProbability()), x - ID_X_OFFSET, y - ID_Y_OFFSET, mIdPaint);
        //canvas.drawText("right eye: " + String.format("%.2f", face.getIsRightEyeOpenProbability()), x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
        //canvas.drawText("left eye: " + String.format("%.2f", face.getIsLeftEyeOpenProbability()), x - ID_X_OFFSET*2, y - ID_Y_OFFSET*2, mIdPaint);

        canvas.drawRect((int)((right-left)/2+left-75), (int)((bottom-top)/3.5+top-33), (int)((right-left)/2+left+75), (int)((bottom-top)/3.5+top+33), mBoxPaint);
        canvas.drawRect((int)((right-left)/1.4+left-30), (int)((bottom-top)/1.5+top-30), (int)((right-left)/1.4+left+30), (int)((bottom-top)/1.5+top+30), mBoxPaint);
        canvas.drawRect((int)((right-left)/3.5+left-30), (int)((bottom-top)/1.5+top-30), (int)((right-left)/3.5+left+30), (int)((bottom-top)/1.5+top+30), mBoxPaint);

        int pixel;
        //int reds=0,greens=0,blues=0;
        int greens=0;

        // koutelo 150*66
        double counter=0.0;

        for(int i=(int)(((right-left)/2+left-75)*0.444); i<(int)(((right-left)/2+left+75)*0.444); i++){
            for(int j=(int)(((bottom-top)/3.5+top-33)*0.444); j<(int)(((bottom-top)/3.5+top+33)*0.444); j++){
                try {
                    pixel = frame.getPixel(640-j, 480-i);
                   // reds += Color.red(pixel);
                    greens += Color.green(pixel);
                   // blues += Color.blue(pixel);
                    counter++;
                } catch (Exception e) {
                   break;
                }
            }
        }

        //double redk= reds/counter,greenk = greens/counter,bluek = blues/counter;
        double greenk = greens/counter;

      //  reds=0;
        greens=0;
      // blues=0;
        counter=0.0;
        // aristero magoulo 60*60
        for(int i=(int)(((right-left)/3.5+left-30)*0.444); i<(int)(((right-left)/3.5+left+30)*0.444); i++){
            for(int j=(int)(((bottom-top)/1.5+top-30)*0.444); j<(int)(((bottom-top)/1.5+top+30)*0.444); j++){
                try {
                    pixel = frame.getPixel(640-j, 480-i);
                   // reds += Color.red(pixel);
                    greens += Color.green(pixel);
                   // blues += Color.blue(pixel);
                    counter++;
                } catch (Exception e) {
                    break;
                }
            }
        }
        //double redam= reds/counter,greenam = greens/counter,blueam = blues/counter;
        double greenam = greens/counter;

        //reds=0;
        greens=0;
       // blues=0;
        counter=0.0;

        // deksi magoulo 60*60
        for(int i=(int)(((right-left)/1.4+left-30)*0.444); i<(int)(((right-left)/1.4+left+30)*0.444); i++){
            for(int j=(int)(((bottom-top)/1.5+top-30)*0.444); j<(int)(((bottom-top)/1.5+top+30)*0.444); j++){
                try {
                    pixel = frame.getPixel(640-j, 480-i);
                    //reds += Color.red(pixel);
                    greens += Color.green(pixel);
                   // blues += Color.blue(pixel);
                    counter++;
                } catch (Exception e) {
                    break;
                }
            }
        }
        //double reddm= reds/counter,greendm = greens/counter,bluedm = blues/counter;
        double greendm = greens/counter;

        double //red = (redam+reddm+redk)/3,
                green = (greenam+greendm+greenk)/3;
              //  blue = (blueam+bluedm+bluek)/3;

        String rgbS = "green( "+ (int)green + ")";
        //FaceTrackerActivity.pulse.setBackgroundColor(Color.rgb((int)red,(int)green,(int)blue));
        FaceTrackerActivity.pulse.setText(rgbS);
        framecnt++;

        if(framecnt >= 784)
            FaceTrackerActivity.plot.setTextColor(Color.GREEN);
        else
            FaceTrackerActivity.plot.setTextColor(Color.RED);

        //redf.add((float)red);
        greenf.add((float)green);
       // bluef.add((float)blue);
    }
}