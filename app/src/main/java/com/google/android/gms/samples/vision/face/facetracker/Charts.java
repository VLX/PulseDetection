package com.google.android.gms.samples.vision.face.facetracker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.R.attr.duration;

public class Charts extends AppCompatActivity {

    public ArrayList<Float> Yf = new ArrayList<>();
    public ArrayList<Float> Gf = new ArrayList<>();
    public ArrayList<Float> ffty = new ArrayList<>();
    int n, m;
    public int fftsize;
    public float max;

    // Lookup tables. Only need to recompute when size of FFT changes.
    Float[] cos;
    Float[] sin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.charts);

        LineChart lineChart4 = (LineChart) findViewById(R.id.charts);
        TextView bps = (TextView) findViewById(R.id.bps);
        G();
        max+=10;
        bps.setText((int)max+" bps");
        // G chart
        ArrayList<Entry> entries = new ArrayList<>();
        for(int i=0; i< fftsize/2; i++){
            entries.add(new Entry(ffty.get(i), i));
        }

        LineDataSet dataset = new LineDataSet(entries, (int)max+" bps");

        ArrayList<String> labels = new ArrayList<String>();
        for(int j=0; j< fftsize/2; j++){
            float sec = j/30.0f;
            DecimalFormat df = new DecimalFormat("##.#");
            labels.add(df.format(sec)+"s");
        }

        LineData data = new LineData(labels, dataset);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        dataset.setDrawCubic(true);
        dataset.setDrawFilled(true);
        dataset.setValueTextSize(0);
        dataset.setCircleSize(0);

        lineChart4.setData(data);
        lineChart4.animateY(5000);
    }

    private void G() {
        float mes = 0.0f;
        float mes2 = 0.0f;
        float sigma;

        for(int i=0; i< FaceGraphic.greenf.size(); i++){
            mes += FaceGraphic.greenf.get(i);
        }
        mes = mes / (FaceGraphic.greenf.size());

        for(int i=0; i< FaceGraphic.greenf.size(); i++){
            Yf.add(FaceGraphic.greenf.get(i) - mes);
        }

        for(int i=0; i< FaceGraphic.greenf.size(); i++){
            mes2 += FaceGraphic.greenf.get(i) * FaceGraphic.greenf.get(i);
        }
        mes2 = mes2 / (FaceGraphic.greenf.size());

        sigma = (float) Math.sqrt(mes2 - (mes * mes));

        for(int i=0; i< FaceGraphic.greenf.size(); i++){
           Gf.add(Yf.get(i)/sigma);
        }

        fftsize = powtwo(Gf.size());

        Float[] rp = new Float[fftsize];
        for(int i=0; i<fftsize; i++){
            rp[i]=Gf.get(i);
        }

        Float[] ip = new Float[fftsize];
        for(int i=0; i<fftsize;i++){
            ip[i]=0.0f;
        }

        fft(rp,ip);

        // peak detection + mo
        float mo =0;
        float peaks = 0;
        max=0;
        for(int i=1; i< ffty.size()-1; i++){
            if(ffty.get(i-1) < ffty.get(i) && ffty.get(i+1) < ffty.get(i)) {
                mo += ffty.get(i);
                peaks++;
            }
        }

        max = mo/peaks;
        max = max*60;
        //Toast.makeText(this.getApplicationContext(), ""+max, Toast.LENGTH_LONG).show();
    }

    public void fft(Float[] x, Float[] y) {
        int i, j, k, n1, n2, a;
        Float c, s, t1, t2;


        // Bit-reverse
        j = 0;
        n2 = n / 2;
        for (i = 1; i < n - 1; i++) {
            n1 = n2;
            while (j >= n1) {
                j = j - n1;
                n1 = n1 / 2;
            }
            j = j + n1;

            if (i < j) {
                t1 = x[i];
                x[i] = x[j];
                x[j] = t1;
                t1 = y[i];
                y[i] = y[j];
                y[j] = t1;
            }
        }

        // FFT
        n1 = 0;
        n2 = 1;

        for (i = 0; i < m; i++) {
            n1 = n2;
            n2 = n2 + n2;
            a = 0;

            for (j = 0; j < n1; j++) {
                c = cos[a];
                s = sin[a];
                a += 1 << (m - i - 1);

                for (k = j; k < n; k = k + n2) {
                    t1 = c * x[k + n1] - s * y[k + n1];
                    t2 = s * x[k + n1] + c * y[k + n1];
                    x[k + n1] = x[k] - t1;
                    y[k + n1] = y[k] - t2;
                    x[k] = x[k] + t1;
                    y[k] = y[k] + t2;
                }
            }
        }

        for (int q=0; q < x.length/2; q++) {
            ffty.add((float) Math.sqrt(x[q]*x[q]+y[q]*y[q]));
        }
    }

    public int powtwo(int size){
        while(!(Math.sqrt((double)size)-(int)Math.sqrt((double)size) == 0)){
            size--;
        }
        return size;
    }
}
